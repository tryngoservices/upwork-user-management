var passport = require('passport');
var keys = require('./keys')
var GoogleStrategy = require("passport-google-oauth").OAuth2Strategy;
var mongoose = require('mongoose');
const UserModel = require('../models/users');
var User = mongoose.model('UserModel');
var jwt = require('express-jwt');
var auth = jwt({
  secret: 'MY_SECRET',
  userProperty: 'payload'
});


passport.serializeUser(function (user, done) {
  done(null, user);
});
passport.deserializeUser(function (user, done) {
  done(null, user);
});
passport.use(
  new GoogleStrategy(
    {
      clientID: keys.googleClientID,
      clientSecret: keys.googleClientSecret,
      callbackURL: "http://sms.webofficeit.com:3001/api/auth/google/callback"
    },
    function (accessToken, refreshToken, profile, done) {
        User.findOne({ email: profile.emails[0].value }).then(existingUser => {
          if (existingUser) {
            // console.log(existingUser)
            token = existingUser.generateJwt();
              var userData = {
                email: profile.emails[0].value,
                name: profile.displayName,
                token: token
              };
            done(null, userData);
          } else {
            var user = new User();
            user.firstname = profile.name['givenName'];
            user.lastname = profile.name['familyName'];
            user.email = profile.emails[0].value;
            var token ='';
            user.save(function (err) {
              token = user.generateJwt();
              var userData = {
                email: profile.emails[0].value,
                name: profile.displayName,
                token: token
              };
              done(null, userData);
            });
          }
        });
      }
  )
);
