import React, { Component } from 'react';
import logo from '../images/logo.png';
import whitelogo from '../images/white-logo.png';
import '../Login/index.css';
import Cookies from 'js-cookie';
class Token extends Component {
    constructor(match, ...props) {
        super(props);
        this.state=({
            boo: ''
        })
    }
    componentDidMount() {
        let foo = localStorage.getItem('token')
        let bar = Cookies.get('token')
        if (foo) {
            this.setState({boo:foo})
            return
        }
        else if(bar){
            console.log(bar)
            this.setState({boo:bar})
            return
        }
        else {
            window.location.href = ('http://sms.webofficeit.com:3000/')
        }
    }
    render() {
        return (
            <div class="main-wrapper">
                <div class="content-container" style={{ height: '84.5%' }}>
                    <div class="l-section  d-flex justify-content-center align-items-center">
                        <div class="max-380 text-center py-4">
                            <img src={whitelogo} alt="" />
                            <p class="mt-4 text-white">
                                Lorem Ipsum is simply dummy text of the printing
                                and typesetting industry. Lorem Ipsum has been
                                the industry's standard dummy
                                text ever since the 1500s,
                        <br /><br />
                                when an unknown printer took a galley
                                of type and scrambled it to make a type
                                specimen book. It has survived not only
                                five centuries.
                    </p>
                        </div>
                    </div>
                    <div class="r-section d-flex justify-content-center">
                        <div class="max-380 text-center py-4">
                            <p class="font-weight-bolder text-secondary">WELCOME TO</p>
                            <img class="mb-3" src={logo} alt="" />
                            <br />
                            <br />
                            <br />
                            <br />
                            <br />
                            <div class="text-center icon-group mb-3">
                                <p class="font-weight-bolder text-secondary">Successfully Sign In</p>
                                {/* Your Token:<br/> 
                                <span style={{"word-break":'break-all'}}>{this.state.boo}</span> */}
                            </div>
                            <div class="flex-nowrap icon-group mb-4">
                                <div class="">
                                    <button type='button' onClick={logOut} class="btn btn-round-attr">Log Out</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
        function logOut() {
            try {
                localStorage.clear();
                Cookies.remove('token');
                window.location.href = ('/');
            }
            catch (error) {
                console.error(error)
            }
        }
    }
}

export default Token;
