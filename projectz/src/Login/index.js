import React, { useState } from 'react';
import logo from '../images/logo.png';
import whitelogo from '../images/white-logo.png';
import socialLogo from '../images/social-logo.png';
import googleLogo from '../images/googleicon.png';
import usericon from '../images/user-icon.png';
import lockicon from '../images/lock-icon.png';
import './index.css';
import {
    Link
} from "react-router-dom";
function Login() {
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const [error, setError] = useState('');
    const [user, setUser] = useState('');
    return (
        <div class="main-wrapper">
            <div class="content-container" style={{ height: '84.5%' }}>
                <div class="l-section  d-flex justify-content-center align-items-center">
                    <div class="max-380 text-center py-4">
                        <img src={whitelogo} alt="" />
                        <p class="mt-4 text-white">
                            Lorem Ipsum is simply dummy text of the printing
                            and typesetting industry. Lorem Ipsum has been
                            the industry's standard dummy
                            text ever since the 1500s,
                        <br /><br />
                            when an unknown printer took a galley
                            of type and scrambled it to make a type
                            specimen book. It has survived not only
                            five centuries.
                    </p>
                    </div>
                </div>
                <div class="r-section d-flex justify-content-center">
                    <div class="max-380 text-center py-4">
                        <p class="font-weight-bolder text-secondary">WELCOME TO</p>
                        <img class="mb-3" src={logo} alt="" />
                        <p class="text-dark mt-3 mb-4">Login to get in the moment updates on the things
                            that intrest you
                    </p>
                        <div class="input-group flex-nowrap icon-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <img src={usericon} alt="" />
                                </span>
                            </div>
                            <input type="text" onChange={event => { setUsername(event.target.value) }} class="form-control" placeholder="Username" aria-label="Username" aria-describedby="addon-wrapping" />
                        </div>
                        <div class="input-group flex-nowrap icon-group mb-4">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <img src={lockicon} alt="" />
                                </span>
                            </div>
                            <input type="password" onChange={event => { setPassword(event.target.value) }} class="form-control" placeholder="Password" aria-label="Username" aria-describedby="addon-wrapping" />
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                {error && <span style={{ color: 'red' }}>Username is not valid</span>}
                                {user && <span style={{ color: 'red' }}>Username not found</span>}
                            </div>
                        </div>
                        <button type="button" class="btn btn-round-attr" onClick={addData}>LOGIN NOW</button>
                        <p class="font-weight-bolder text-secondary mt-3">Don’t have an account ? <Link to='/signup'><a class="text-pink " href="">Sign UP Now</a></Link> </p>
                        <div class="d-flex align-items-center w-75 m-auto">
                            <i class="border-top w-100 my-2"></i><b class="mx-3 text-secondary">Or</b><i class="border-top w-100 my-2"></i>
                        </div>
                        <p class="font-weight-bolder text-secondary mt-3">Continue with social media</p>
                        <div class="text-center">
                            <img src={socialLogo} alt=""></img>&nbsp;&nbsp;
                            <a href='http://sms.webofficeit.com:3001/api/auth/google'><img  class='img' src={googleLogo} alt=""></img></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
    function validateEmail(testusername) {
        var re = /^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/;
        return re.test(testusername);
    }
    function addData() {
        var usernameboolean = validateEmail(username);
        if (usernameboolean) {
            setError(false);
            try {
                fetch('http://sms.webofficeit.com:3001/api/login', {
                    method: 'post',
                    body: JSON.stringify({
                        email: username,
                        password: password,
                    }),
                    headers: {
                        "Content-type": "application/json; charset=UTF-8"
                    }
                }).then(response => {
                    if (response.status == '200') {
                            response.json().then((token) => {
                                localStorage.setItem('token',token['token'])
                                window.location.href = '/token/';
                            })
                    } else {
                        setUser(true);
                    }
                });
            }
            catch (error) {
                console.error(error)
            }
        }
        else {
            setError(true);
        }
    }
}

export default Login;
