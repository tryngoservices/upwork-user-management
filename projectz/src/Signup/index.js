import React, { useState } from 'react';
import './index.css';
import {
    Link
} from "react-router-dom";

function SignUp() {
    const [firstname, setFirstName] = useState('');
    const [lastname, setLastName] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [error, setError] = useState('');
    const [account, setAccount] = useState('');
    return (
        <div class="col-md-12" style={{ height: '100%' }} >
            <div style={{ height: '25%' }}></div>
            <div class="row" style={{ height: '50%' }}>
                <div class='col-md-3'></div>
                <div class='col-md-6'>
                    <div class='row'>
                        <h3 class="col-md-12 mb-4" align="center"> Sign up</h3>
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="input-group">
                                    <input type="text" onChange={event => { setFirstName(event.target.value) }} class="form-control" placeholder=" First name*" required />
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="input-group">
                                    <input type="text" onChange={event => { setLastName(event.target.value) }} class="form-control" placeholder="Last name*" required />
                                </div>
                            </div>

                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <input type="email" onChange={event => { setEmail(event.target.value) }} class="form-control" placeholder="Email Address*" required />
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <input type="password" onChange={event => { setPassword(event.target.value) }} class="form-control" placeholder="Password*" required="" />
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                {error && <span style={{ color: 'red' }}>Invalid Fields</span>}
                                {account && <span style={{ color: 'green' }}>Account Created</span>}
                            </div>
                        </div>
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-custom1 btn-block" onClick={SignUp}>SIGN UP</button>
                        </div>
                        <div class='col-md-12'>
                            <div class='row'>
                                <div class='col-md-7'></div>
                                <div class='col-md-5 pull-right' style={{ flex: 1, width: '100%', alignSelf: 'flex-end', justifyContent: 'flex-end' }}>
                                    <Link to='/'> <a href=''>Already Have an account? Sign In</a></Link>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class='col-md-3'></div>
            <div style={{ height: '25%' }}></div>
        </div>
    );
    function validateEmail(testemail) {
        var re = /^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/;
        return re.test(testemail);
    }
    function validateName(testname) {
        var re = /^[a-zA-Z]+(([',. -][a-zA-Z ])?[a-zA-Z]*)*$/;
        return re.test(testname);
    };
    function SignUp() {
        var emailboolean = validateEmail(email)
        var firstnameboolean = validateName(firstname)
        var lastnameboolean = validateName(lastname)
        if (emailboolean && firstnameboolean && lastnameboolean) {
            setError(false);
            try {
                fetch('http://sms.webofficeit.com:3001/api/register', {
                    method: 'post',
                    body: JSON.stringify({
                        firstname: firstname,
                        lastname: lastname,
                        email: email,
                        password: password,
                    }),
                    headers: {
                        "Content-type": "application/json; charset=UTF-8"
                    }
                }).then(response => response.json().then(token => {
                    setAccount(true);
                    window.location.href = ('/');
                }))
            }
            catch (error) {
                console.error(error)
            }
        }
        else {
            setError(true);
        }
    }
}

export default SignUp;
