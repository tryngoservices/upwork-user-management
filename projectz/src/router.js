import React, { Component } from "react";
import {
  Switch,
  BrowserRouter as Router,
  Route,
} from "react-router-dom";
import Login from './Login/index';
import SignUp from './Signup/index';
import Token from "./Token";

export default class App extends Component {
  render(){
  return (
    <Router>
      <Switch>
          <Route exact path="/" >
            <Login />
          </Route>
          <Route exact path="/signup">
            <SignUp />
          </Route>
          <Route path="/:token" component={Token} />
            
          </Switch>
    </Router>
  );
}
}