const express = require('express');
var passport = require('passport');
var mongoose = require('mongoose');
var cookie = require('cookie');
const router = express.Router();
const UserModel = require('../models/users');
var User = mongoose.model('UserModel');
require('../config/googleauth')
require('../config/passport')

var jwt = require('express-jwt');
var auth = jwt({
    secret : 'MY_SECRET',
    userProperty : 'payload'
});
/* GET Google Authentication API. */
router.get('/auth/google',passport.authenticate("google", {
    scope: [
        "https://www.googleapis.com/auth/userinfo.profile",
        "https://www.googleapis.com/auth/userinfo.email"
      ]}));

router.get('/auth/google/callback',
    passport.authenticate("google", { failureRedirect: "/", session: false }),
    function(req, res) {
        console.log(req)
        var token = req.user.token;
        res.cookie('token',token);
        res.redirect('http://sms.webofficeit.com:3000/token/?token='+token);
        res.status(200);
    }
);
router.post('/login', function(req,res){
    passport.authenticate('local',function(err,user,info){
        var token;
        //if Password throws/catches an error
        if (err){
    
            res.status(404).json(err)
            return;
        }
        //if User is found
        if(user){
            token = user.generateJwt();
            res.status(200);
            res.json({
                'token':token
            });
        }
        else{
            //If user is not found  
            res.status(401).json(info)
            }
    })(req,res);
});
router.get('/test', function(req,res){
    res.send(200);
})
router.post('/register', function(req,res){
    var user = new User();
    user.firstname = req.body.firstname;
    user.lastname = req.body.lastname;
    user.email = req.body.email;
    user.setPassword(req.body.password);

    user.save(function(err){
        var token;
        token = user.generateJwt();
        res.status(200);
        res.json({
            'token': token
        });
    });
});
///Authentication Module


module.exports = router;