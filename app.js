var express = require('express')
var cookieParser = require('cookie-parser')
var cookie = require('cookie');
var mongoose = require('mongoose')
var bodyparser = require('body-parser')
var cors = require('cors')
var path = require('path')
var passport = require('passport');
require('./models/users');
require('./config/passport');
require('./config/googleauth');
var app = express()

const route = require('./routes/route' )

///Connect to Mongoo DB
mongoose.connect('mongodb://127.0.0.1:27017/projectz',{useCreateIndex:true ,useNewUrlParser:true, useUnifiedTopology: true });

mongoose.connection.on('connected',()=>{
    console.log('Connected to database');
});
mongoose.connection.on('error',(err)=>{
    if(err){
        console.log('Error'+err);
    }
    
});
///Adding Middleware - Cors
app.use(cors());
///
app.use(cookieParser())
/// Body-Parser
app.use(bodyparser.json())
///
////Static Foler
app.use(express.static(path.join(__dirname,'public')))

app.use(passport.initialize());

app.use('/api', route)
app.use(function(err,req,res,next){
    if(err.name === 'Unauthorized Error'){
        res.status(401);
        res.json({
            'message': err.name +": "+ err.message
        })
    }
});

const port = '3001'

app.listen(port,()=>{
    console.log('server started at port: '+port)
})